#!/bin/bash
HOME=/home/ec2-user
APP=$HOME/code-deploy-demo
LOG=$HOME/deploy.log

# This example assumes the sample 'my_app.sh' script has been added to the the directory 'my_app' and serves as the application launcher

/bin/echo "$(date '+%Y-%m-%d %X'): ** Application Start Hook Started **" >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): Event: $LIFECYCLE_EVENT" >> $LOG
