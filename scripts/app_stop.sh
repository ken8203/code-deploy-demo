#!/bin/bash

HOME=/home/ec2-user
APP=$HOME/code-deploy-demo
PIDF=$APP/code-deploy-demo.pid
LOG=$HOME/deploy.log

/bin/echo "$(date '+%Y-%m-%d %X'): ****************************************************************" >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): Initializing Deployment for $APPLICATION_NAME - $DEPLOYMENT_ID " >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): ****************************************************************" >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): ** Application Stop Hook Started **" >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): Event: $LIFECYCLE_EVENT" >> $LOG
