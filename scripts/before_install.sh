#!/bin/bash
HOME=/home/ec2-user
APP=$HOME/code-deploy-demo
LOG=$HOME/deploy.log
/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Started **" >> $LOG

# Do some actions before the installation
/usr/bin/sudo /bin/mkdir -p /var/www/html/code-deploy-demo
/usr/bin/sudo /bin/chown -R ec2-user:ec2-user /var/www/html/code-deploy-demo

/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Completed **" >> $LOG